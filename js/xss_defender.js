function xss_defender(str_row) {
    var new_div = document.createElement("div");
    new_div.innerHTML = str_row;
    var content = new_div.innerText;
    return str_row;
    //return htmlUnEscape(str_row); 
}

//定义一个转换htmlh还原特殊字符的方法
function htmlUnEscape(str) {
  return str.replace(/&lt;|&gt;|&quot;|&amp;/g, (match) => {
    switch (match) {
      case '&lt;':
        return '<';
      case '&gt;':
        return '>';
      case '&quot;':
        return '"';
      case '&amp;':
        return '&';
    }
});
}
