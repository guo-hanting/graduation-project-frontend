$(function () {

    layui.use("layer", function () {
        var layer = layui.layer;
        var link;
        var config = {
            type: 2
            , title: false //不显示标题栏
            , closeBtn: 2
            , area: ['48vw', '55vh']
            , shade: 0.8
            , id: 'LAY_layuipro' //设定一个id，防止重复弹出
            , btn: ['查看详情', '我知道了']
            , btnAlign: 'c'
            , moveType: 1 //拖拽模式，0或者1
            , content: ''
            , success: function (layero) {
                var btn = layero.find('.layui-layer-btn');
                btn.find('.layui-layer-btn0').attr({
                    href: link
                    , target: '_blank'
                });
            }
        }

        var active = {
            notice: function () {
                //示范一个公告层
                layer.open(config);
            }
        }

        $('.footer .my-layer').on('click', function () {
            var othis = $(this), method = othis.data('method');
            config.title = $(this).text();
            config.content = "/announce/" + $(this).attr("page");
            link = "/announce/" + $(this).attr("page");
            active[method] ? active[method].call(this, othis) : '';
        });

        //多窗口模式 - esc 键
        $(document).on('keyup', function (e) {
            if (e.keyCode === 27) {
                // layer.close(layer.escIndex ? layer.escIndex[0] : 0);
                layer.closeAll('iframe');
            }
        });
    })

})